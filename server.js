var express =   require("express");
var multer  =   require('multer');
var http    =   require('http');
var path    =   require('path');
var app     =   express();
var fs      =   require('fs');
var rimraf  =   require('rimraf');
var io      =   require('socket.io');
var watershed = require('msk-watershed');
var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './uploads');
  },
  filename: function (req, file, callback) {
    callback(null, file.fieldname + '-' + Date.now());
}
});
    var upload = multer({ storage : storage}).single('userPhoto');
    var server =http.createServer(app).listen(3000);
    io = io.listen(server);
// use files from the public folder
    app.use(express.static('public'));
    // use the .ejs file
    app.set('view engine', 'ejs');
    // acquire the html file
    app.get('/',function(req,res){
      res.sendFile(__dirname + "/index.html");
    });
        var counter = 0;

        // redirect to things server
        app.post('/api',function(req,res){
    // deleting the folder
        rimraf('./uploads', function(err) {
        if (err) {  throw err; }
    // creating the folder
        fs.mkdir('./uploads', function() {
            upload(req,res,function(err) {
                if(err) {
                    return res.end("Error uploading file.");
                }
    // getting files in the folder
                var fileList = [];
            //    console.log('before');
                var files = fs.readdirSync('./uploads');
            //    console.log('read');
                for(var i in files){
                    if (!files.hasOwnProperty(i)) continue;
            //        console.log('other');
                    var name = 'uploads/'+files[i];
                    if (!fs.statSync(name).isDirectory()){
                        fileList.push(name);
                    }
                };
    // using the watershed module to convert the images
                watershed.ws(fileList[0],'./public/images/result'+ counter +'.png')
                .then(function(){
                   var path = 'result'+ counter +'.png';

                    res.render("./result.ejs", {img_path: path});
                    counter++;
                });



        // //     var path = fileList[0];
        //
        //         var path = 'b0.png';
        //
        //         res.render("./result.ejs", {img_path: path});

            });
        });
    });
});
// io.on('connection', function(socket) {
//     socket.emit('news', {
//         hello: 'world'
//     });
//         socket.on('my other event', function() {
//             console.log("conected!");
//         });
//
//     //    io.on('connection', function(socket) {
//         socket.on('message', function () {
//             console.log('Message Received:');
//             //socket.broadcast.emit('message', msg);
//             fs.readFile(__dirname + 'path', function(err, buf){
//                 console.log('before');
//                 if (err) {
//                     throw err;
//                 } else {
//
//     // it's possible to embed binary data
//     // within arbitrarily-complex objects
//             socket.emit('image', { image: true, buffer: buf.toString('base64') });
//             //socket.emit('image', { image: true, buffer: buf });
//             console.log('image file is initialized');
//         }
//         });
//     //});
// });
//     socket.on('disconnect', function(data) {
//     console.log('disconnect!');
//    });
// });
